
prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Main.cpp Arbolbalanceado.cpp Grafo.cpp
OBJ = Main.o Arbolbalanceado.o Grafo.o
APP = Main

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

.PHONY: install	