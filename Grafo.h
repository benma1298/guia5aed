
#ifndef GRAFO_H
#define GRAFO_H

#include <fstream>
#include <iostream>
#include "Arbolbalanceado.h"
using namespace std;

class Grafo{
    private:

    public:
    Grafo();
    void GenerarGrafo(Nodo *p);
    void PreOrden(Nodo *a, ofstream &fp);
};

#endif