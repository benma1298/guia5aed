
#include <fstream>
#include <iostream>
#include "Grafo.h"
using namespace std;

Grafo::Grafo(){

}

/* */
void Grafo::GenerarGrafo(Nodo *ArbolInt) {

	ofstream fp;
	fp.open("grafo.txt");

	fp << "digraph G {" << endl;
	fp << "node [style=filled fillcolor=yellow];" << endl;
	fp << "nullraiz [shape=point];" << endl; 
	fp << "nullraiz->"<<ArbolInt->info <<"[label="<<ArbolInt->FE<<"];" << endl; 
  
	PreOrden(ArbolInt, fp);
	fp << "}" << endl;
	fp.close();
  
	system("dot -Tpng -ografo.png grafo.txt &");
	system("eog grafo.png &");
}

/* */
void Grafo::PreOrden(Nodo *a, ofstream &fp) {
  
  
	if (a != NULL) {
		if (a->izq != NULL) {
			fp << a->info <<"->"<<a->izq->info<< " [label="<<a->izq->FE<<"];"<<endl; 
		} else{
			fp << '"'<< a->info << "i" <<'"'<<" [shape=point];"<<endl;
			fp<<a->info<<"->"<<'"'<< a->info << "i" <<'"'<<";"<<endl; 
		}
		if (a->der != NULL) {
			fp << a->info <<"->"<<a->der->info<< " [label="<<a->der->FE<<"];"<<endl;  
		} else{
			fp << '"'<< a->info << "d" <<'"'  <<" [shape=point];"<<endl;
			fp<< a->info<<"->"<<'"'<< a->info << "d" <<'"'<<";"<<endl; 
		}
		PreOrden(a->izq,fp);
		PreOrden(a->der,fp); 
	}
}
