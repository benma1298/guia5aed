
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include "Arbolbalanceado.h"
#include "Grafo.h"

using namespace std;

int main(int argc, char **argv){
    int opcion;
    int elemento;

    
    Arbolbalanceado *arbol = new Arbolbalanceado();
    Nodo *raiz = NULL;
    Grafo *g = new Grafo();

    
    system("clear");
    opcion = arbol -> Menu();
    bool inicio;

    while(opcion){
        switch(opcion){

            case 1:
            cout << "Ingresar Proteina: " << endl;
            cin >> elemento;
            inicio = false;
            arbol -> Insertar(raiz, inicio, elemento);
            g -> GenerarGrafo(raiz);
            break;

            case 2:
            cout << "Eliminar Proteina: " << endl;
            cin >> elemento;
            inicio = false;
            arbol -> Eliminar(raiz, inicio, elemento);
            g -> GenerarGrafo(raiz);
            break;

            case 3:
            g -> GenerarGrafo(raiz);
            break;

            case 4:
            cout << "Buscar Proteina: " << endl;
            cin >> elemento;
            arbol -> Buscar(raiz, elemento);
            break;

            case 0:
            exit(0);
        }
        opcion = arbol -> Menu();
    }
    return 0;
}