
#ifndef ARBOLBALANCEADO_H
#define ARBOLBALANCEADO_H

#include <iostream>
using namespace std;

// Se defina la estructura del nodo.
typedef struct Nodo{
    struct Nodo *der;
    struct Nodo *izq;
    int FE;
    int info;
} Nodo;

class Arbolbalanceado{
    private:
    
    public:
    Arbolbalanceado();
    int Menu();
    void Insertar(Nodo *&nodocabeza, bool BO, int infor);
    void Buscar(Nodo *nodo, int infor);
    void Restructura1(Nodo *&nodocabeza, bool BO);
    void Restructura2(Nodo *&nodocabeza, bool BO);
    void Borra(Nodo *&aux1, Nodo *&otro1, bool BO);
    void Eliminar(Nodo *&nodocabeza, bool BO, int infor);
};

#endif