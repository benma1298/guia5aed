
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include "Arbolbalanceado.h"
using namespace std;

Arbolbalanceado::Arbolbalanceado(){

}
// Se crea funcion de Menu con opciones correspondientes.
int Arbolbalanceado::Menu(){
    int num;
    do{
        cout << "1.- Insertar.\n" << endl;
        cout << "2.- Eliminar.\n" << endl;
        cout << "3.- Generar Grafo.\n" << endl;
        cout << "4.- Buscar.\n" << endl;
        cout << "0.- Finalizar Programa.\n" << endl;
        cout << "Eliga una opcion: " << endl;
        cin >> num;
    } while(num < 0 || num > 7 );
    return num;
}

void Arbolbalanceado::Insertar(Nodo *&nodocabeza, bool BO, int infor) {
	
	Nodo *nodo = NULL;
	Nodo *nodo1 = NULL;
	Nodo *nodo2 = NULL; 
  
	nodo = nodocabeza;
  
	if (nodo != NULL) {
    
		if (infor < nodo->info) {
			Insertar(nodo->izq, BO, infor);
      
			if(BO) {
        
				switch (nodo->FE) {
					case 1: 
						nodo->FE = 0;
						BO = false;
						break;
          
					case 0: 
						nodo->FE = -1;
						break;
     
					case -1: 
					/* reestructuración del árbol */
						nodo1 = nodo->izq;
            
				/* Rotacion II */
						if (nodo1->FE <= 0) { 
							nodo->izq = nodo1->der;
							nodo1->der = nodo;
							nodo->FE = 0;
							nodo = nodo1;
            
						} else { 
				/* Rotacion ID */
							nodo2 = nodo1->der;
							nodo->izq = nodo2->der;
							nodo2->der = nodo;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;
         
							if (nodo2->FE == -1){
								nodo->FE = 1;
							}else{
								nodo->FE =0;
							}
					
							if (nodo2->FE == 1){
								nodo1->FE = -1;
							}else {
								nodo1->FE = 0;
							}
					
							nodo = nodo2;
						}
            
						nodo->FE = 0;
						BO = false;
						break;
				}
			} 
      
		} else {	
      
			if (infor > nodo->info) {
				Insertar(nodo->der, BO, infor);
        
				if (BO) {
			
					switch (nodo->FE) {
            
						case -1: 
							nodo->FE = 0;
							BO = false;
							break;
           
						case 0: 
							nodo->FE = 1;
							break;
     
						case 1: 
			 			/* reestructuración del árbol */
							nodo1 = nodo->der;
              
							if (nodo1->FE >= 0) { 
								/* Rotacion DD */
								nodo->der = nodo1->izq;
								nodo1->izq = nodo;
								nodo->FE = 0;
								nodo = nodo1;
                
							} else { 
									/* Rotacion DI */
								nodo2 = nodo1->izq;
								nodo->der = nodo2->izq;
								nodo2->izq = nodo;
								nodo1->izq = nodo2->der;
								nodo2->der = nodo1;
                
								if (nodo2->FE == 1){
									nodo->FE = -1;
								}else{
									nodo->FE = 0;
								}
       
								if (nodo2->FE == -1){
									nodo1->FE = 1;
								}else{
									nodo1->FE = 0;
								}				
								nodo = nodo2;
							}
              
							nodo->FE = 0;
							BO = false;
							break;
					}	
				}
			} else {
				cout << "Ahora el nodo se encuentra en el árbol" << endl;
			}
		}
	} else {
    
		nodo = (struct Nodo*) malloc (sizeof(Nodo));
		nodo->izq = NULL;
		nodo->der = NULL;
		nodo->info = infor;
		nodo->FE = 0;
		BO = true;
	}
  
	nodocabeza = nodo;
}

/* */
void Arbolbalanceado::Buscar(Nodo *nodo, int infor) {
	if (nodo != NULL) {
		if (infor < nodo->info) {
			Buscar(nodo->izq,infor);
		} else {
			if (infor > nodo->info) {
				Buscar(nodo->der,infor);
			} else {
				cout << "El nodo SI se encuentra en el árbol\n";
			}
		}
	} else {
		cout << "El nodo NO se encuentra en el árbol\n";
	}
}

/* */
void Arbolbalanceado::Restructura1(Nodo *&nodocabeza, bool BO) {
	Nodo *nodo, *nodo1, *nodo2; 
	nodo = nodocabeza;
  
	if (BO) {
		switch (nodo->FE) {
			case -1: 
				nodo->FE = 0;
				break;
      
			case 0: 
				nodo->FE = 1;
				BO = false;
				break;
   
			case 1: 
				/* reestructuracion del árbol */
				nodo1 = nodo->der;
      
				if (nodo1->FE >= 0) { 
				/* rotacion DD */
					nodo->der = nodo1->izq;
					nodo1->izq = nodo;
        
					switch (nodo1->FE) {
						case 0: 
							nodo->FE = 1;
							nodo1->FE = -1;
							BO = false;
							break;
						case 1: 
							nodo->FE = 0;
							nodo1->FE = 0;
							BO = false;
							break;           
					}
					nodo = nodo1;
				} else { 
					/* Rotacion DI */
					nodo2 = nodo1->izq;
					nodo->der = nodo2->izq;
					nodo2->izq = nodo;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;
       
					if (nodo2->FE == 1){
						nodo->FE = -1;
					}else{
						nodo->FE = 0;
					}
						
					if (nodo2->FE == -1){
						nodo1->FE = 1;
					}else{
						nodo1->FE = 0;
					}
					
					nodo = nodo2;
					nodo2->FE = 0;       
				} 
				break;   
		}		
	}
	nodocabeza=nodo;
}

/* */
void Arbolbalanceado::Restructura2(Nodo *&nodocabeza, bool BO) {
	Nodo *nodo, *nodo1, *nodo2; 	
	nodo = nodocabeza;
  
	if (BO) {
		switch (nodo->FE) {
			case 1: 
				nodo->FE = 0;
				break;
			case 0: 
				nodo->FE = -1;
				BO = false;
				break;
			case -1: 
				/* reestructuracion del árbol */
				nodo1 = nodo->izq;
				if (nodo1->FE<=0) { 
					/* rotacion II */
					nodo->izq = nodo1->der;
					nodo1->der = nodo;
					switch (nodo1->FE) {
						case 0: 
							nodo->FE = -1;
							nodo1->FE = 1;
							BO = false;
							break;
						case -1: 
							nodo->FE = 0;
							nodo1->FE = 0;
							BO = false;
							break;
					}
					nodo = nodo1;
				} else { 
					/* Rotacion ID */
					nodo2 = nodo1->der;
					nodo->izq = nodo2->der;
					nodo2->der = nodo;
					nodo1->der = nodo2->izq;
					nodo2->izq = nodo1;
       
						if (nodo2->FE == -1){
							nodo->FE = 1;
						}else{
							nodo->FE = 0;
						}
        
						if (nodo2->FE == 1){
							nodo1->FE = -1;
						}else{
							nodo1->FE = 0;
						}
       
						nodo = nodo2;
						nodo2->FE = 0;       
				}      
				break;   
		}
	}
	nodocabeza = nodo;
}


/* */
void Arbolbalanceado::Borra(Nodo *&aux1, Nodo *&otro1, bool BO) {
	Nodo *aux, *otro; 
	aux=aux1;
	otro=otro1;
  
	if (aux->der != NULL) {
		Borra(aux->der,otro,BO); 
		Restructura2(aux,BO);
	} else {
		otro->info = aux->info;
		aux = aux->izq;
		BO = true;
	}
	aux1=aux;
	otro1=otro;
}

/* */
void Arbolbalanceado::Eliminar(Nodo *&nodocabeza, bool BO, int infor) {
	Nodo *nodo, *otro; 
	
	nodo = nodocabeza;
  
	if (nodo != NULL) {
		if (infor < nodo->info) {
			Eliminar(nodo->izq, BO, infor);
			Restructura1(nodo,BO);
		} else {
			if (infor > nodo->info) {
				Eliminar(nodo->der, BO, infor);
				Restructura2(nodo,BO); 
			} else {
				otro = nodo;
				if (otro->der == NULL) {
					nodo = otro->izq;
					BO = true;     
				} else {
					if (otro->izq==NULL) {
						nodo=otro->der;
						BO=true;     
					} else {
						Borra(otro->izq,otro,BO);
						Restructura1(nodo,BO);
						free(otro);
					}
				}
			}
		} 
	} else {
		cout << "El nodo NO se encuentra en el árbol\n" << endl;
	}
	nodocabeza=nodo;
}
